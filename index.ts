import { Application, Router, send } from 'https://deno.land/x/oak/mod.ts';
import * as path from 'https://deno.land/std@0.57.0/path/mod.ts';
import { parse } from 'https://deno.land/std@0.117.0/flags/mod.ts';
import { getId, getTemplatePath, getUser, installHandlebars, log, tunnel } from './utils.ts';
import { getPackagedFile } from './utils.ts';
import { nanoid } from 'https://deno.land/x/nanoid/mod.ts';

const app = new Application();

installHandlebars(app, { viewRoot: './view', viewExt: '.handlebars' });

const args = parse(Deno.args, {
  default: {
    count: Infinity,
  },
});
args.file = args.file || args._[0];
args.message = args.message || args.m;

// TODO: either host handlebars templates on github or find a way to embed them so deno compile works
// TODO: should set up p2p connection to send files

const [user, share] = await Promise.all([getUser(), getPackagedFile(args.file)]);
const message = args.message ?? `${user.username} shared ${share.filename}`;

log('');
log(`Share by link <u><blue>http://${tunnel.url}</blue></u>`);
log(`<dim>locally <u>http://localhost:8000</u>`);
log('');

const router = new Router();

router.get(`/`, async ctx => {
  let id = await getId(ctx);
  if (!id) {
    id = nanoid(5);
    ctx.cookies.set(`identifier`, id);
  }
  const template = await getTemplatePath();
  await ctx.render(template, { user, share, message });
});

router.get('/preview', async ctx => {
  await send(ctx, path.basename(share.absolutePath), {
    root: path.dirname(share.absolutePath),
  });
});

router.get('/pixel', async ctx => {
  const id = await getId(ctx);
  log(`<dim>[ ${id} ] opened link</dim>`);
  ctx.response.status = 200;
});

router.post(`/decide`, async ctx => {
  const id = await getId(ctx);
  const query = ctx.request.body({ type: 'form' });
  const data = await query.value;

  log(`<dim>[ ${id} ] started download</dim>`);
  ctx.response.headers.set(`content-disposition`, `attachment; filename="${share.filename}"`);

  await send(ctx, path.basename(share.absolutePath), {
    root: path.dirname(share.absolutePath),
  });
  log(`<green>[ ${id} ] finished download</green>`);
});

app.use(router.routes());
app.use(router.allowedMethods());

await app.listen({ port: 8000 });
