# Deno Share

using the power of deno and ngrok you can share files from your terminal to a friends web browser.  if you have deno installed simply run `./build.sh` and then you can copy `./dist/dshare` to your bin folder.

now that it's installed you can share a file or folder

`dshare ~/Documents`

and share the link 

optionally you can add a message to show on the resulting page with `-m "Hey friend, here is friendly message"

to stop sharing, simply use ctrl+c