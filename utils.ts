import { lookup } from 'https://deno.land/x/media_types@v2.11.0/mod.ts';
import { Ngrok } from 'https://deno.land/x/ngrok@3.1.1/mod.ts';
import { prettyBytes } from 'https://deno.land/x/pretty_bytes@v1.0.5/mod.ts';
import * as path from 'https://deno.land/std@0.57.0/path/mod.ts';
import username from 'https://deno.land/x/username/mod.ts';

import * as ViewEngine from 'https://deno.land/x/view_engine@v1.4.5/mod.ts';
import type { Application } from 'https://deno.land/x/oak/mod.ts';
import type { ViewConfig } from 'https://deno.land/x/view_engine@v1.4.5/mod.ts';
import { zipDir } from 'https://deno.land/x/jszip/mod.ts';
import * as ink from 'https://deno.land/x/ink/mod.ts';

export const tunnel = await getTunnel();

async function exit(code?: number) {
  await tunnel.disconnect();
  Deno.exit(code ?? 0);
}

Deno.addSignalListener('SIGINT', exit);

export function log(txt: string) {
  console.log(ink.colorize(txt));
}

export function error(txt: string) {
  log(`<red>${txt}</red>`);
  exit(1);
}

export async function packageFolder(absolutePath: string) {
  let tmpPath = await Deno.makeTempDir();
  log('<dim>Packaging folder for share</dim>');

  let newPath = path.join(tmpPath, path.basename(absolutePath) + '.zip');
  let zip = await zipDir(absolutePath);
  await zip.writeZip(newPath);

  Deno.addSignalListener('SIGINT', async () => {
    log('<dim>Cleaning up packaging</dim>');
    await Deno.remove(tmpPath, { recursive: true });
  });

  return newPath;
}

export async function getId(ctx) {
  return await ctx.cookies.get('identifier');
}

export async function getPackagedFile(relativePath: string) {
  if (!relativePath?.length)
    return error('Path file was not provided.  for ex: dshare ~/Documents');
  let absolutePath = path.resolve(relativePath);

  let stat = await Deno.stat(absolutePath);
  if (stat.isDirectory) {
    absolutePath = await packageFolder(absolutePath);
    stat = await Deno.stat(absolutePath);
  }

  const fileSize = stat.size;
  const filename = path.basename(absolutePath);
  const contentType = lookup(absolutePath) || '';
  const preview = contentType.includes('image') && `/preview`;

  return {
    absolutePath,
    filename,
    fileSize,
    contentType,
    preview,
  };
}

export async function getTunnel() {
  const ngrok = await Ngrok.create({ protocol: 'http', port: 8000 });
  const ngrokEvent = await new Promise<any>(resolve => ngrok.addEventListener('ready', resolve));

  return {
    url: ngrokEvent.detail,
    disconnect: () => ngrok.destroy('SIGKILL').catch(() => {}),
  };
}

export async function getUser() {
  return {
    username: await username(),
  };
}

declare module 'https://deno.land/x/oak/mod.ts' {
  interface Context {
    render: (fileName: string, data?: object) => void;
  }

  interface RouterContext {
    render: (fileName: string, data?: object) => void;
  }

  interface Application {
    view: ViewConfig;
  }
}

export async function getTemplatePath() {
  const defaultPath = `./view/index.handlebars`;
  try {
    await Deno.stat(path.resolve(defaultPath));
    return defaultPath;
  } catch (e) {
    return 'https://gitlab.com/Omarzion/dshare/-/raw/main/view/index.handlebars';
  }
}

export function installHandlebars(app: Application, opts: ViewConfig) {
  const handlebarsEngine = ViewEngine.engineFactory.getHandlebarsEngine();
  const oakAdapter = ViewEngine.adapterFactory.getOakAdapter();

  app.use(ViewEngine.viewEngine(oakAdapter, handlebarsEngine));
  return app;
}
