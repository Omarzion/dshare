export const index = `
<html lang='en'>
  <head>
    <meta charset='UTF-8' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0' />
    <meta http-equiv='X-UA-Compatible' content='ie=edge' />
    <meta property='og:title' content='{{share.filename}}' />
    <meta property='og:type' content='{{share.contentType}}' />
    <meta property='og:description' content='{{user.username}} shared {{share.filename}}' />
    {{#if share.preview}}
      <meta property='og:image' content='{{share.preview}}' />
    {{/if}}
    <link
      href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css'
      rel='stylesheet'
      integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3'
      crossorigin='anonymous'
    />
    <style>
    </style>
    <script src='/pixel'></script>
    <title>File share</title>
  </head>
  <body>
    <form action='/decide' method='post'>
      <div class='modal d-block' style='display: static;' tabindex='-1'>
        <div style='margin-top: 20vh'></div>
        <div class='modal-dialog modal-lg'>
          <div class='modal-content'>
            <div class='modal-header'>
              <h5 class='modal-title'>{{share.filename}}
                <span class='text-secondary fw-light'>({{share.fileSize}})</span></h5>
            </div>
            <div class='modal-body'>
              {{#if share.preview}}
                <p>
                  <img src='{{share.preview}}' class='img-fluid' alt='{{share.filename}}' />
                </p>
              {{/if}}
              <p>{{message}}</p>
            </div>
            <div class='modal-footer'>
              <button name='option' value='accept' class='btn btn-primary'>
                Download
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </body>
</html>
`;
